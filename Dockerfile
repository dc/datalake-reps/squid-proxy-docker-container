# -*- docker-image-name: "squid_proxy_ecap_http_violation" -*-
# Datalake squid proxy base image. Provides the base image for each datalake local access point squid
FROM centos:7
MAINTAINER pkramp <p.n.kramp@gsi.de>
RUN yum update -y
WORKDIR /tmp/squid-4.7
RUN yum install scp perl gcc gcc-c++ autoconf automake wget libxml3-devel libxml2 curl libcap-devel libtool-ltdl-devel libecap-devel make cmake crypto -y
RUN wget http://www.squid-cache.org/Versions/v4/squid-4.7.tar.gz
RUN tar -xzf squid-4.7.tar.gz
RUN squid-4.7/configure --enable-http-violations --prefix=/usr --includedir=/usr/include --datadir=/usr/share --bindir=/usr/sbin --libexecdir=/usr/lib/squid  --localstatedir=/var --sysconfdir=/etc/squid --enable-ecap --disable-dependency-tracking --disable-dependency-tracking --enable-eui --enable-follow-x-forwarded-for --enable-auth --enable-auth-basic=DB,NCSA,NIS,POP3,RADIUS --enable-auth-ntlm=fake --enable-auth-digest=file --enable-external-acl-helpers=file_userip,unix_group,wbinfo_group --enable-http-violations  --enable-ecap --enable-cache-digests --enable-cachemgr-hostname=localhost --enable-delay-pools --enable-epoll --enable-ident-lookups --enable-linux-netfilter --enable-removal-policies=heap,lru --enable-snmp --enable-storeio=aufs,diskd,rock,ufs --enable-wccpv2 --enable-ecap --with-aio --with-default-user=squid --with-dl --with-pthreads --disable-arch-native build_alias=x86_64-redhat-linux-gnu host_alias=x86_64-redhat-linux-gnu
RUN time make -j8
ENTRYPOINT ["cp"]
CMD ["-r","-a","/tmp/squid-4.7","/vol/squid-4.7"]
