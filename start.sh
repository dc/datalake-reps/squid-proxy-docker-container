!/bin/bash
docker rm squidbuilder &>/dev/null
docker build --tag squidbuilder:0.1 --tag squidbuilder:latest .
docker run -v $PWD/build:/vol  --name "squidbuilder" -i -t squidbuilder:latest
